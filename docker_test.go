package docker_test

import (
	"context"
	"encoding/base64"
	"os/exec"
	"strings"
	"testing"
)

type Shell interface {
	Execute(ctx context.Context, cmd string) (output []byte, err error)
}

type LocalShell struct{}

func (sh LocalShell) Execute(ctx context.Context, cmd string) ([]byte, error) {
	wrapperCmd := exec.CommandContext(ctx, "sh", "-c", cmd)
	return wrapperCmd.CombinedOutput()
}

func TestContainer_ExecuteCurl(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'curl --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "Protocols: ") {
		t.Errorf("expected output to contain protocols, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "Release-Date: ") {
		t.Errorf("expected output to contain release-date, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "Features: ") {
		t.Errorf("expected output to contain features, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "curl ") {
		t.Errorf("expected output to contain curl version, got:\n%s", string(output))
	}
}

func TestContainer_ExecuteGit(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'git --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "git version ") {
		t.Errorf("expected output to contain git version, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePingVersion(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'ping -V'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "ping from iputils ") {
		t.Errorf("expected output to contain ping version, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePingLocalhost(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'ping localhost -c 3'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), " bytes from localhost ") {
		t.Errorf("expected output to contain number of bytes from localhost, got:\n%s", string(output))
	}
}

func TestContainer_ExecuteJq(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'jq --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "jq-") {
		t.Errorf("expected output to contain jq version, got:\n%s", string(output))
	}
}

func TestContainer_ExecuteCatLsbRelease(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'cat /etc/lsb-release'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "DISTRIB_ID=") {
		t.Errorf("expected output to contain DISTRIB_ID, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "DISTRIB_RELEASE=") {
		t.Errorf("expected output to contain DISTRIB_RELEASE, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "DISTRIB_CODENAME=") {
		t.Errorf("expected output to contain DISTRIB_CODENAME, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "DISTRIB_DESCRIPTION=") {
		t.Errorf("expected output to contain DISTRIB_DESCRIPTION, got:\n%s", string(output))
	}
}

func TestContainer_ExecuteUnzip(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'unzip -v'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "UnZip ") {
		t.Errorf("expected output to contain UnZip, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "UnZip special compilation options:") {
		t.Errorf("expected output to contain UnZip special compilation options, got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "UnZip and ZipInfo environment options:") {
		t.Errorf("expected output to contain UnZip and ZipInfo environment options, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePython3Version(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'python3 --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "Python ") {
		t.Errorf("expected output to contain Python, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePython3HelloWorld(t *testing.T) {
	shell := LocalShell{}
	// encoded for quotes: docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'python3 -c "print(\"Hello World\");"'
	cmd, _ := base64.StdEncoding.DecodeString("ZG9ja2VyIHJ1biAtLXJtIC0tZW50cnlwb2ludD1iYXNoIHJlZ2lzdHJ5LmdpdGxhYi5jb20vamxlc2tpL2F6dXJlLWRldm9wcy1hZ2VudDpsYXRlc3QtdWJ1bnR1MjAgLWwgLWMgJ3B5dGhvbjMgLWMgInByaW50KFwiSGVsbG8gV29ybGRcIik7Iic=")
	output, err := shell.Execute(context.Background(), string(cmd))
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "Hello World") {
		t.Errorf("expected output to contain Hello World, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePipVersion(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'pip --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "pip ") {
		t.Errorf("expected output to contain pip version, got:\n%s", string(output))
	}
}

func TestContainer_ExecutePipList(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'pip list'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "Package             Version") {
		t.Errorf("expected output to contain \"Package             Version\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteWhichPython(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'which python'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "/usr/bin/python") {
		t.Errorf("expected output to contain \"/usr/bin/python\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteAzVersion(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'az version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "azure-cli") {
		t.Errorf("expected output to contain \"azure-cli\", got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "azure-cli-core") {
		t.Errorf("expected output to contain \"azure-cli-core\", got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "azure-cli-telemetry") {
		t.Errorf("expected output to contain \"azure-cli-telemetry\", got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "extensions") {
		t.Errorf("expected output to contain \"extensions\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteKubectlVersion(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'kubectl version --client=true --output=json'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "clientVersion") {
		t.Errorf("expected output to contain \"clientVersion\", got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "gitVersion") {
		t.Errorf("expected output to contain \"gitVersion\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteKubelogin(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'kubelogin --version'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "kubelogin version") {
		t.Errorf("expected output to contain \"kubelogin version\", got:\n%s", string(output))
	}
	if !strings.Contains(string(output), "git hash") {
		t.Errorf("expected output to contain \"git hash\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteEffectiveUidName(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'id -un'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "agent") {
		t.Errorf("expected output to contain \"agent\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteEffectiveGidName(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'id -gn'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "agent") {
		t.Errorf("expected output to contain \"agent\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteHomeExists(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'echo $HOME && test -d $HOME'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "/home/agent") {
		t.Errorf("expected output to contain \"/home/agent\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteHomeWritable(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'touch $HOME/foo && ls $HOME/foo'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "/home/agent/foo") {
		t.Errorf("expected output to contain \"/home/agent/foo\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteAgentRootExists(t *testing.T) {
	shell := LocalShell{}
	_, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'test -d /azp'")
	if err != nil {
		t.Fatal(err)
	}
}

func TestContainer_ExecuteAgentRootWritable(t *testing.T) {
	shell := LocalShell{}
	output, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'touch /azp/foo && ls /azp/foo'")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(output), "/azp/foo") {
		t.Errorf("expected output to contain \"/azp/foo\", got:\n%s", string(output))
	}
}

func TestContainer_ExecuteAgentStartShExecutable(t *testing.T) {
	shell := LocalShell{}
	_, err := shell.Execute(context.Background(), "docker run --rm --entrypoint=bash registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -l -c 'test -x /azp/start.sh'")
	if err != nil {
		t.Fatal(err)
	}
}

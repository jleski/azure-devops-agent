#!/usr/bin/env bash

echo "Building Azure DevOps Agent (Ubuntu 18)..."
podman build -t registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu18 -f Dockerfile.ubuntu18 .
echo "Building Azure DevOps Agent (Ubuntu 20)..."
podman build -t registry.gitlab.com/jleski/azure-devops-agent:latest-ubuntu20 -f Dockerfile.ubuntu20 .
go test docker_test.go